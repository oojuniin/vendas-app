import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from 'src/app/models/client';
import { ClientLink } from 'src/app/models/client-link';
import { Response } from 'src/app/models/response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ClienteService {
  private readonly apiUrl = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  findAll(): Observable<Response<ClientLink>> {
    return this.http.get<Response<ClientLink>>(
      `${this.apiUrl}/client/find/all`
    );
  }

  findAllWithPagination(page: number): Observable<Response<ClientLink>> {
    return this.http.get<Response<ClientLink>>(
      `${this.apiUrl}/client/find/all?page=${page}`
    );
  }

  findByCpf(cpf: string): Observable<Response<ClientLink>> {
    return this.http.get<Response<ClientLink>>(
      `${this.apiUrl}/client/find/cpf/${cpf}`
    );
  }

  findByName(name: string): Observable<Response<ClientLink>> {
    return this.http.get<Response<ClientLink>>(
      `${this.apiUrl}/client/find/name/${name}`
    );
  }

  findById(id: number): Observable<Response<ClientLink>> {
    return this.http.get<Response<ClientLink>>(
      `${this.apiUrl}/client/find/id/${id}`
    );
  }

  save(client: Client): Observable<Response<ClientLink>> {
    return this.http.post<Response<ClientLink>>(
      `${this.apiUrl}/client/save`,
      client
    );
  }

  update(client: Client): Observable<Response<ClientLink>> {
    return this.http.put<Response<ClientLink>>(
      `${this.apiUrl}/client/update`,
      client
    )
  }

  deleteById(id: number): Observable<Response<Client>> {
    return this.http.delete<Response<Client>>(
      `${this.apiUrl}/client/delete/id/${id}`
    );
  }
}
