export const environment = {
  production: true,
  LOGIN_URL: 'https://vendas-api-oojuniin.herokuapp.com/api/login',
  API_URL: 'https://vendas-api-oojuniin.herokuapp.com/api/v1',
};
