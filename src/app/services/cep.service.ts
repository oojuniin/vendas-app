import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {CepResponse} from "../models/cepResponse";

@Injectable({
    providedIn: 'root'
})
export class CepService {
    private readonly apiUrl = `https://viacep.com.br/ws/`;

    constructor(private http: HttpClient) {
    }

    get(cep: string): Observable<CepResponse> {
        return new Observable((x)=>{
            let request = new XMLHttpRequest();
            request.open('get', `${this.apiUrl}/${cep}/json`);
            request.send();
            request.onload = function () {
                let data = JSON.parse(this.response);
                x.next(data)
            }
        })
    }
}
