import { HttpHeaders } from "@angular/common/http";
import { Error } from "./error";

export interface ResponseError {
  error: Error;
  headers: HttpHeaders;
  message: string;
  name: string;
  ok: boolean;
  status: number;
  statusText: string;
  url: string
}
