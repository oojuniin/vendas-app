import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroTransportadoraComponent } from './cadastro-transportadora/cadastro-transportadora.component';



@NgModule({
  declarations: [
    CadastroTransportadoraComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CadastroTransportadoraComponent
  ]
})
export class TransportadoraModule { }
