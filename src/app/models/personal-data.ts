import { Address } from './address';

export interface PersonalData {
  name: string | '';
  birthday: string | '';
  registrationDate: string | '';
  cpf: string | '';
  rg: string | '';
  email: string | '' | null;
  address: Address;
  phoneNumberOne: string | '';
  phoneNumberTwo: string | '';
  comments: string | '';
}
