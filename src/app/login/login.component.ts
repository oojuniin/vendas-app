import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseError } from '../models/response-error';

import { Token } from '../models/token';
import { LoginService } from './login.service';
import { TokenStorageService } from './token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  hide = true;
  message = '';
  errorMessage = '';
  isLoginFailed: boolean = false;

  constructor(
    private loginService: LoginService,
    private tokenStorageService: TokenStorageService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    })
  }

  login(): void {
    const loginData = {
      username: this.loginForm.get('username')?.value,
      password: this.loginForm.get('password')?.value
    }
    this.loginService.singin(loginData).subscribe(
      (data: Token) => {
        this.tokenStorageService.saveAccessToken(data.access_token)
        this.tokenStorageService.saveUser(data);
        this.router.navigate(['home']).then(() => {
          window.location.reload();
        });
      },
      (error: ResponseError) => {
        if (error.status === 403) {
          this.errorMessage = `E-mail ou senha inválido`;
        }
        this.isLoginFailed = true;
      }
    );
  }
}
