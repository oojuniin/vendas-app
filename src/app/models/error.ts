export interface Error {
    statusCode: number;
    detail: string;
    type: string;
    status: number;
    timestamp: string;
    developerMessage: string;
    message: string;
    objectName: string;
    errors: string[];
}
