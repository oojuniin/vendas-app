import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendasComponent } from './vendas.component';

@NgModule({
  declarations: [
    VendasComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    VendasComponent
  ]
})
export class VendasModule { }
