import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClienteModule} from './cliente/cliente.module';
import {AppRoutingModule} from '../app-routing.module';
import {EstoqueModule} from './estoque/estoque.module';
import {FornecedorModule} from './fornecedor/fornecedor.module';
import {FuncionarioModule} from './funcionario/funcionario.module';
import {PerfilModule} from './perfil/perfil.module';
import {ProdutoModule} from './produto/produto.module';
import {TransportadoraModule} from './transportadora/transportadora.module';
import {VendasModule} from './vendas/vendas.module';

import {HomeComponent} from './home/home.component';
import {MenuComponent} from './menu/menu.component';

import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatToolbarModule} from "@angular/material/toolbar";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BsDropdownModule} from "ngx-bootstrap/dropdown";


@NgModule({
  declarations: [
    HomeComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    ClienteModule,
    EstoqueModule,
    FornecedorModule,
    FuncionarioModule,
    PerfilModule,
    ProdutoModule,
    TransportadoraModule,
    VendasModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    BrowserAnimationsModule,
    BsDropdownModule
  ],
  exports: [
    HomeComponent,
    MenuComponent
  ]
})
export class NavegacaoModule { }
