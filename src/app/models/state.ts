export interface State {
  id: number | null;
  uf: string;
  name: string;
}
