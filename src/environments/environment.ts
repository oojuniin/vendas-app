export const environment = {
  production: false,
  LOGIN_URL: 'http://localhost:8080/api/login',
  API_URL: 'http://localhost:8080/api/v1',
};
