import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {City} from '../models/city';
import {Response} from '../models/response';

@Injectable({
    providedIn: 'root',
})
export class CityService {
    private readonly apiUrl = `${environment.API_URL}`;

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Response<City>> {
        return this.http.get<Response<City>>(`${this.apiUrl}/city/find/all`);
    }

    findByName(cityName: string): Observable<Response<City>> {
        return this.http.get<Response<City>>(
            `${this.apiUrl}/city/find/name/${cityName}`
        );
    }

    findByUf(uf: string): Observable<Response<City>> {
        return this.http.get<Response<City>>(`${this.apiUrl}/city/find/uf/${uf}`);
    }
}
