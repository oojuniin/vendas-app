import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cpf'
})
export class CpfPipe implements PipeTransform {

  transform(cpf: string, ...args: unknown[]): any {
    if (cpf) {
      const value = cpf.toString().replace(/\D/g, '');

      let cpfFormatado = '';

      cpfFormatado = value.replace(/(\d{3})?(\d{3})?(\d{3})?(\d{2})/, '$1.$2.$3-$4');

      return cpfFormatado;
    }
  }

}
