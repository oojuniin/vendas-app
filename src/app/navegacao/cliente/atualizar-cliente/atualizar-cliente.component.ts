import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { CepResponse } from 'src/app/models/cepResponse';
import { City } from 'src/app/models/city';
import { Client } from 'src/app/models/client';
import { ClientLink } from 'src/app/models/client-link';
import { Response } from 'src/app/models/response';
import { ResponseError } from 'src/app/models/response-error';
import { State } from 'src/app/models/state';
import { CepService } from 'src/app/services/cep.service';
import { StateService } from 'src/app/services/state.service';

import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-atualizar-cliente',
  templateUrl: './atualizar-cliente.component.html',
  styleUrls: ['./atualizar-cliente.component.scss']
})
export class AtualizarClienteComponent implements OnInit {
  atualizarClientForm!: FormGroup;
  client!: Client;
  cities: City[] = [];
  states: State[] = [];
  detail: string = '';
  cepResponse: CepResponse = {
    uf: '',
    logradouro: '',
    cep: '',
    bairro: '',
    localidade: '',
    complemento: '',
    ddd: 0,
    gia: 0,
    ibge: 0,
    siafi: 0
  };

  horizontalPosition: MatSnackBarHorizontalPosition = 'end'
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(
    private stateService: StateService,
    private clientService: ClienteService,
    private cepService: CepService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AtualizarClienteComponent>,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) private data: Client) { }

  ngOnInit(): void {
    this.client = this.data;
    this.buildform();
    this.findAllStates();
  }

  getCep() {
    if (this.atualizarClientForm.get('cep')?.value.length == 8) {
      this.cepService.get(this.atualizarClientForm.get('cep')?.value).subscribe(
        (response: CepResponse) => {
          this.findStateByUf(response.uf);
          this.atualizarClientForm.get('city')?.setValue(response.localidade)
          this.atualizarClientForm.get('district')?.setValue(response.bairro)
          this.atualizarClientForm.get('street')?.setValue(response.logradouro)
        },
        (error: any) => {
          console.log(error);
        }
      )
    }
  }

  private findStateByUf(uf: string) {
    this.stateService.findByUf(uf).subscribe(
      (response: Response<State>) => {
        this.atualizarClientForm.get('state')?.setValue(response.data[0].name);
      },
      (error: ResponseError) => {
        this.openSnackBar(`${error.error.detail} - ${error.status}`, 'mat-warn')
        console.log(error);
      }
    );
  }

  update() {
    if (this.atualizarClientForm.dirty && this.atualizarClientForm.valid) {
      const client: Client = {
        id: this.atualizarClientForm.get('id')?.value,
        personalData: {
          address: {
            complemento: this.atualizarClientForm.get('complemento')?.value,
            state: this.atualizarClientForm.get('state')?.value,
            city: this.atualizarClientForm.get('city')?.value,
            district: this.atualizarClientForm.get('district')?.value,
            number: this.atualizarClientForm.get('number')?.value,
            street: this.atualizarClientForm.get('street')?.value,
            cep: this.atualizarClientForm.get('cep')?.value,
          },
          name: this.atualizarClientForm.get('name')?.value,
          birthday: moment(this.atualizarClientForm.get('birthday')?.value).format('YYYY-MM-DD').toString(),
          comments: this.atualizarClientForm.get('comments')?.value,
          cpf: this.atualizarClientForm.get('cpf')?.value,
          email: this.atualizarClientForm.get('email')?.value,
          phoneNumberOne: this.atualizarClientForm.get('phoneNumberOne')?.value,
          phoneNumberTwo: this.atualizarClientForm.get('phoneNumberTwo')?.value,
          registrationDate: '',
          rg: this.atualizarClientForm.get('rg')?.value,
        },
      };

      /*
       * outra forma de atribuir os itens, mas da erro de null...
       * this.client = Object.assign({}, this.client, this.atualizarClientForm.value);
       */

      this.clientService.update(client).subscribe(
        (response: Response<ClientLink>) => {
          if (response.status === 'OK') {
            this.openSnackBar(`Cliente atualizado com sucesso!`, 'mat-primary')
            setTimeout(function () {
              window.location.reload();
            }, 1000)
          }
        },
        (error: ResponseError) => {
          this.openSnackBar(`${error.error.detail} - ${error.status}`, 'mat-warn')
          console.log(error);
        }
      );
    } else {
      this.openSnackBar(`Preencha o formulário corretamente.`, 'mat-warn');
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  private openSnackBar(message: string, cor: string) {
    this._snackBar.open(message, '', {
      panelClass: ['mat-toolbar', cor],
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
    setTimeout(() => {
      this._snackBar.dismiss();
    }, 5000)
  }

  private findAllStates() {
    this.stateService.findAll().subscribe(
      (response: Response<State>) => {
        this.states = response.data;
      },
      (error: ResponseError) => {
        console.log(error);
      }
    );
  }

  private buildform() {
    this.atualizarClientForm = this.formBuilder.group({
      id: [this.client.id],
      email: [this.client.personalData.email, [Validators.email, Validators.required]],
      name: [this.client.personalData.name, [Validators.required]],
      cpf: [this.client.personalData.cpf, [Validators.required, Validators.pattern('[0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2}')]],
      rg: [this.client.personalData.rg, [Validators.required]],
      phoneNumberOne: [this.client.personalData.phoneNumberOne, [Validators.required]],
      phoneNumberTwo: [this.client.personalData.phoneNumberTwo],
      birthday: [this.client.personalData.birthday, [Validators.required]],
      registrationDate: [this.client.personalData.registrationDate],
      state: [this.client.personalData.address.state, [Validators.required]],
      city: [this.client.personalData.address.city, [Validators.required]],
      district: [this.client.personalData.address.district, [Validators.required]],
      street: [this.client.personalData.address.street, [Validators.required]],
      number: [this.client.personalData.address.number, [Validators.required]],
      cep: [this.client.personalData.address.cep, [Validators.required]],
      comments: [this.client.personalData.comments],
    });
  }
}
