import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhonePipe } from './pipes/phone.pipe';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { CpfPipe } from './pipes/cpf.pipe';

@NgModule({
  declarations: [
    PhonePipe,
    DateFormatPipe,
    CpfPipe
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    PhonePipe,
    DateFormatPipe,
    CpfPipe
  ]
})
export class SharedModule { }
